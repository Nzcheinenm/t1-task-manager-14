package ru.t1.dkononov.tm.api.controllers;

public interface IProjectTaskController {
    void bindTaskToProject();

    void unbindTaskFromProject();
}
