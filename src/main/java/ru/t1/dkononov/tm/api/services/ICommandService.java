package ru.t1.dkononov.tm.api.services;

import ru.t1.dkononov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
